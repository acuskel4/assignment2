package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static
     * fields.
     * This field is a logger. Loggers are like a more advanced println, for writing
     * messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does
     * something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void boxCompleteDetectsCompletedBoxes() {
        // code to test that boxComplete returns true for boxes that *are complete*
        // A grid where we're going to draw a box, and then test whether it's complete
        DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(5, 5, 2);
        case1.drawHorizontal(0, 0, 0);
        case1.drawVertical(0, 0, 1);
        case1.drawHorizontal(0, 1, 0);
        case1.drawVertical(1, 0, 1);

        assertTrue(case1.boxComplete(0, 0));

    }

    @Test
    public void boxCompleteDetectsIncompleteBoxes() {
        // code to test that boxComplete returns false for boxes that *are not complete*

        // A grid where we're going to draw 1 side of a box, and then test whether it's
        // complete
        DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(5, 5, 2);
        case1.drawHorizontal(0, 0, 0);

        assertFalse(case1.boxComplete(0, 0));
    }

    @Test
    public void drawMethodsDetectRedrawnLines() {
        // code to test that drawHorizontal throws an exception if the line was already
        // drawn
        // code to test that drawVertical throws an exception if the line was already
        // drawn goes here
        DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(5, 5, 2);

        // player 1 draws a horizontal line over the line player 0 has already drawn
        case1.drawHorizontal(0, 0, 0);
        assertThrows(IllegalStateException.class, () -> {
            case1.drawHorizontal(0, 0, 1);
        });

        // players 1 draws a vertical line over the line player 0 has already drawn
        case1.drawVertical(0, 0, 0);
        assertThrows(IllegalStateException.class, () -> {
            case1.drawVertical(0, 0, 1);
        });
    }

}
